//
//  DetailViewController.swift
//  IndividualProject
//
//  Created by user203105 on 10/31/21.
//

import UIKit

class DetailViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet var nameField: UITextField!
    @IBOutlet var artField: UITextField!
    @IBOutlet var albumField: UITextField!
    @IBOutlet var lengthField: UITextField!
    
    @IBAction func backgroundTapped(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    @IBOutlet weak var buttonFav: UIButton!
    
    // Favorite button
    @IBAction func favoriteButton(_ sender: UIButton) {
        if song.favorite == false {
            sender.setImage(UIImage(named: "heart"), for: .normal)
            song.favorite = true
            print("Set favorite: \(song.favorite)")
        } else {
            sender.setImage(UIImage(named: "notheart"), for: .normal)
            song.favorite = false
            print("Set favorite: \(song.favorite)")
        }
    }
    
    // Button disabled
    @IBAction func choosePhotoSource(_ sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let photoLibraryAction = UIAlertAction(title: "Photo Library", style: .default) {_ in
            print("Present photo library")
        }
        alertController.addAction(photoLibraryAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func deletePopUp(_ sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: nil, message: "Are you sure you want to delete \(song.name)", preferredStyle: .alert)
        alertController.modalPresentationStyle = .formSheet
        alertController.popoverPresentationController?.barButtonItem = sender
        
        let deleteAction = UIAlertAction(title: "Yes", style: .destructive) {
            _ in
            print("Deleting item")
            
            self.allSongs.removeItem(self.song)
            self.navigationController!.popViewController(animated: true)
        }
        
        alertController.addAction(deleteAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    var song: Song! {
        didSet{
            navigationItem.title = song.name
        }
    }
    var allSongs: SongStore!
    
    let numberFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .none
        formatter.maximumIntegerDigits = 3
        formatter.minimumIntegerDigits = 2
        return formatter
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        nameField.text = song.name
        artField.text = song.artist
        albumField.text = song.album
        lengthField.text = numberFormatter.string(from: NSNumber(value: song.length))
        
        if song.favorite == false {
            buttonFav.setImage(UIImage(named: "notheart"), for: .normal)
        } else {
            buttonFav.setImage(UIImage(named: "heart"), for: .normal)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Clear first responder
        view.endEditing(true)
        
        // "Save" changes to the song
        song.name = nameField.text ?? ""
        song.artist = artField.text ?? ""
        song.album = albumField.text ?? ""
        
        if let valueText = lengthField.text,
           let value = numberFormatter.number(from: valueText) {
            song.length = value.intValue
        } else {
            song.length = 0
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
