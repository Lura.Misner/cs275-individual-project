//
//  Song.swift
//  IndividualProject
//
//  Created by user203105 on 10/26/21.
//

import UIKit

class Song: Equatable, Codable {
    var name: String
    var artist: String
    var album: String
    var length: Int
    var favorite: Bool
    
    init(name: String, artist: String, album: String, length: Int) {
        self.name = name
        self.artist = artist
        self.album = album
        self.length = length
        self.favorite = false
    }
    
    convenience init(random: Bool = false) {
        if random {
            let songNames = ["Bad Habits", "Memory", "I See You", "Happier Than Ever", "LOST IN THE CITADEL"]
            let songArt = ["Ed Sheeran", "Kane Brown", "MISSIO", "Billie Eilish", "Lil Nas X"]
            let albums = ["=", "No Album", "The Darker The Weather", "Happier Than Ever", "Montero"]
            let length = [231, 153, 229, 299, 171]
            
            let r = Int.random(in: 0..<5)
            
            self.init(name: songNames[r], artist: songArt[r], album: albums[r], length: length[r])
            
        } else {
            self.init(name: "", artist: "", album: "", length: 0)
        }
    }
    
    static func ==(lh: Song, rh: Song) -> Bool {
        return lh.name == rh.name
        && lh.artist == rh.artist
        && lh.album == rh.album
        && lh.length == rh.length
    }
}
