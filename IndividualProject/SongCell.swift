//
//  SongCell.swift
//  IndividualProject
//
//  Created by user203105 on 10/27/21.
//

import UIKit

class SongCell: UITableViewCell {
    
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var artistLabel: UILabel!
    @IBOutlet var lengthLabel: UILabel!
    
}
