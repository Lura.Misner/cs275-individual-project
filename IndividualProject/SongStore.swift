//
//  SongStore.swift
//  IndividualProject
//
//  Created by user203105 on 10/26/21.
//

import UIKit

class SongStore {
    var allSong = [Song]()
    
    let songArchiveURL: URL = {
        let documentsDirectories = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDictionary = documentsDirectories.first!
        return documentDictionary.appendingPathComponent("songs.plist")
    }()
    
    @discardableResult func createItem() -> Song {
        let newSong = Song(random: true)
        allSong.append(newSong)
        return newSong
    }
    
    func removeItem(_ song: Song) {
        if let index = allSong.firstIndex(of: song) {
            allSong.remove(at: index)
        }
    }
    
    func moveItem(from fromIndex: Int, to toIndex: Int) {
        if fromIndex == toIndex {
            return
        }
        
        let movedItem = allSong[fromIndex]
        allSong.remove(at: fromIndex)
        allSong.insert(movedItem, at: toIndex)
    }
    
    @objc func saveChanges() -> Bool {
        print("Saving songs to: \(songArchiveURL)")
        do {
            let encoder = PropertyListEncoder()
            let data = try encoder.encode(allSong)
            try data.write(to: songArchiveURL, options: [.atomic])
            print("Saved all of the songs")
            return true
        } catch let encodingError {
            print("Error encoding allSong: \(encodingError)")
            return false
        }
    }
    
    init() {
        do {
            let data = try Data(contentsOf: songArchiveURL)
            let unarchiver = PropertyListDecoder()
            let items = try unarchiver.decode([Song].self, from: data)
            if items.count > 0 {
                allSong = items
            } else {
                // Create some items because its empty
                for _ in 0...2 {
                    createItem()
                }
            }
        } catch {
            print("Error reading in saved songs: \(error)")
            
            // Create some items because its empty
            for _ in 0...2 {
                createItem()
            }
        }
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(saveChanges), name: UIScene.didEnterBackgroundNotification, object: nil)
    }
}
