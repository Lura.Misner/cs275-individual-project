//
//  ViewController.swift
//  IndividualProject
//
//  Created by user203105 on 10/26/21.
//

import UIKit

class SongViewController: UITableViewController {
    var songStore: SongStore!
    
    @IBAction func addNewItem(_ sender: UIBarButtonItem) {
        let newItem = songStore.createItem()
        
        if let index = songStore.allSong.firstIndex(of: newItem) {
            let indexPath = IndexPath(row:index, section: 0)
            
            // Insert new row into table
            tableView.insertRows(at: [indexPath], with: .automatic)
            
        }
    }
    
    required init?(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)
        navigationItem.leftBarButtonItem = editButtonItem
    }
    
    
    override func tableView(_ tableView: UITableView,
                            numberOfRowsInSection section: Int) -> Int {
        return songStore.allSong.count
    }
    
    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SongCell",
                                                 for: indexPath) as! SongCell
        let song = songStore.allSong[indexPath.row]
        
        cell.nameLabel.text = song.name
        cell.artistLabel.text = song.artist
        
        let minute = song.length / 60
        let sec = song.length % 60
        cell.lengthLabel.text = "\(minute):\(sec)"
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let song = songStore.allSong[indexPath.row]
            
            let alertController = UIAlertController(title: nil, message: "Are you sure you want to delete \(song.name)", preferredStyle: .alert)
            alertController.modalPresentationStyle = .formSheet
            
            let deleteAction = UIAlertAction(title: "Yes", style: .destructive) {
                _ in
                print("Deleting item")
                self.songStore.removeItem(song)
                tableView.deleteRows(at: [indexPath], with: .automatic)
            }
            
            alertController.addAction(deleteAction)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
            
            present(alertController, animated: true, completion: nil)
        }
    }
    
    override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        songStore.moveItem(from: sourceIndexPath.row, to: destinationIndexPath.row)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 75
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        // If the triggered segue is the "showItem" segue
        switch segue.identifier {
        case "showItem":
            // Figure out which row was just tapped
            if let row =  tableView.indexPathForSelectedRow?.row {
                // Get the item associated with this row and pass it along
                let item = songStore.allSong[row]
                let detailViewController = segue.destination as! DetailViewController
                
                detailViewController.song = item
                detailViewController.allSongs = songStore
            }
        default:
            preconditionFailure("Unexpected segue identifier.")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
}

